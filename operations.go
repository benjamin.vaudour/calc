package calc

import (
	"framagit.org/benjamin.vaudour/number/v2"
)

type Op1Func func(number.Number) number.Number
type Op2Func func(number.Number, number.Number) number.Number

func (c *Calc) Op1(cb Op1Func) error {
	n, err := c.s.Pop()
	if err == nil {
		c.Push(cb(n))
	} else {
		c.Reset()
	}
	return err
}

func (c *Calc) Op2(cb Op2Func) error {
	n1, n2, err := c.s.Pop2()
	if err == nil {
		c.Push(cb(n1, n2))
	} else {
		c.Reset()
	}
	return err
}

func (c *Calc) ToBase(b int) error {
	cb := func(n number.Number) number.Number {
		return number.ToBase(n, b)
	}
	return c.Op1(cb)
}

func (c *Calc) ToInt(b ...int) error {
	cb := func(n number.Number) number.Number {
		return number.ToInt(n, b...)
	}
	return c.Op1(cb)
}

func (c *Calc) ToDec(b ...int) error {
	cb := func(n number.Number) number.Number {
		return number.ToDec(n, b...)
	}
	return c.Op1(cb)
}

func (c *Calc) ToFrac(b ...int) error {
	cb := func(n number.Number) number.Number {
		return number.ToFrac(n, b...)
	}
	return c.Op1(cb)
}

func (c *Calc) ToSci(b ...int) error {
	cb := func(n number.Number) number.Number {
		return number.ToSci(n, b...)
	}
	return c.Op1(cb)
}

func (c *Calc) Abs() error {
	return c.Op1(number.Abs)
}

func (c *Calc) Neg() error {
	return c.Op1(number.Neg)
}

func (c *Calc) Inc() error {
	cb := func(n number.Number) number.Number {
		return number.Add(n, number.I(1))
	}
	return c.Op1(cb)
}

func (c *Calc) Dec() error {
	cb := func(n number.Number) number.Number {
		return number.Sub(n, number.I(1))
	}
	return c.Op1(cb)
}

func (c *Calc) Add() error {
	return c.Op2(number.Add)
}

func (c *Calc) Sub() error {
	return c.Op2(number.Sub)
}

func (c *Calc) Mul() error {
	return c.Op2(number.Mul)
}

func (c *Calc) Div() error {
	return c.Op2(number.Div)
}

func (c *Calc) Quo() error {
	return c.Op2(number.Quo)
}

func (c *Calc) Rem() error {
	return c.Op2(number.Rem)
}

func (c *Calc) Inv() error {
	return c.Op1(number.Inv)
}

func (c *Calc) Pow() error {
	return c.Op2(number.Pow)
}

func (c *Calc) Sqrt() error {
	return c.Op1(number.Sqrt)
}

func (c *Calc) Sqrtn() error {
	return c.Op2(number.Sqrtn)
}

func (c *Calc) Fact() error {
	return c.Op1(number.Fact)
}

func (c *Calc) Lsh() error {
	return c.Op2(number.Lsh)
}

func (c *Calc) Rsh() error {
	return c.Op2(number.Rsh)
}

func (c *Calc) Cmp() error {
	return c.Op2(number.Cmp)
}

func (c *Calc) _c(cb func(number.Number, number.Number) bool) error {
	cb2 := func(n1, n2 number.Number) number.Number {
		if cb(n1, n2) {
			return number.I(1)
		}
		return number.I(0)
	}
	return c.Op2(cb2)
}

func (c *Calc) Eq() error {
	return c._c(number.Eq)
}

func (c *Calc) Ne() error {
	return c._c(number.Ne)
}

func (c *Calc) Gt() error {
	return c._c(number.Gt)
}

func (c *Calc) Le() error {
	return c._c(number.Le)
}

func (c *Calc) Ge() error {
	return c._c(number.Ge)
}

func (c *Calc) Lt() error {
	return c._c(number.Lt)
}

func (c *Calc) Sum() error {
	if c.s.IsEmpty() {
		return ErrIsEmpty(errStack)
	}
	stk := c.Stack()
	s := stk[0]
	for _, n := range stk[1:] {
		s = number.Add(s, n)
	}
	c.Reset()
	c.Push(s)
	return nil
}

func (c *Calc) Len() error {
	c.Push(number.I(int64(c.s.Len())))

	return nil
}

func (c *Calc) Min() error {
	if c.s.IsEmpty() {
		return ErrIsEmpty(errStack)
	}
	stk := c.Stack()
	m := stk[0]
	for _, n := range stk[1:] {
		if number.Lt(n, m) {
			m = n
		}
	}
	c.Reset()
	c.Push(m)
	return nil
}

func (c *Calc) Max() error {
	if c.s.IsEmpty() {
		return ErrIsEmpty(errStack)
	}
	stk := c.Stack()
	m := stk[0]
	for _, n := range stk[1:] {
		if number.Gt(n, m) {
			m = n
		}
	}
	c.Reset()
	c.Push(m)
	return nil
}

func (c *Calc) Mean() (err error) {
	l := c.s.Len()
	if err = c.Sum(); err == nil {
		c.Push(number.I(int64(l)))
		err = c.Div()
	}
	return
}

func (c *Calc) Median() (err error) {
	l := c.s.Len()
	if l == 0 {
		return ErrIsEmpty(errStack)
	}
	stk := c.Stack()
	stk.Sort()
	var m number.Number
	if l&1 == 0 {
		i := l >> 1
		m = number.Div(number.Add(stk[i], stk[i-1]), number.I(2))
	} else {
		m = stk[l>>1]
	}
	c.s.Reset()
	c.Push(m)
	return nil
}

func (c *Calc) Mode() (err error) {
	l := c.s.Len()
	if l == 0 {
		return ErrIsEmpty(errStack)
	}
	stk := c.Stack()
	m := make(map[number.Number]int)
loop:
	for _, n := range stk {
		for k := range m {
			if number.Eq(k, n) {
				m[k]++
				continue loop
			}
		}
		m[n] = 1
	}
	i := 0
	var n number.Number
	for k, j := range m {
		if j > i {
			n, i = k, j
		}
	}
	c.Reset()
	c.Push(n)
	return
}

func (c *Calc) Variance() (err error) {
	stk := c.Stack()
	if err = c.Mean(); err != nil {
		return
	}
	m, _ := c.s.Pop()
	p2 := number.I(2)
	for _, n := range stk {
		c.Push(number.Pow(number.Sub(n, m), p2))
	}
	c.Sum()
	c.Push(number.I(int64(stk.Len())))
	return c.Div()
}

func (c *Calc) StdDeviation() (err error) {
	if err = c.Variance(); err == nil {
		err = c.Sqrt()
	}
	return
}
