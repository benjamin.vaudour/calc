package calc

import (
	"framagit.org/benjamin.vaudour/number/v2"
)

func Push(c *Calc, numbers ...number.Number) { c.Push(numbers...) }
func Pop(c *Calc) error                      { return c.Pop() }
func Reset(c *Calc) error                    { return c.Reset() }
func Duplicate(c *Calc) error                { return c.Duplicate() }
func DuplicateStack(c *Calc) error           { return c.DuplicateStack() }
func Reverse(c *Calc) error                  { return c.Reverse() }
func ReverseStack(c *Calc) error             { return c.ReverseStack() }
func Sort(c *Calc) error                     { return c.Sort() }
func SortReverse(c *Calc) error              { return c.SortReverse() }

func ResetRegistry(c *Calc, r string) error { return c.ResetRegistry(r) }
func ResetRegistries(c *Calc) error         { return c.ResetRegistries() }
func Store(c *Calc, r string) error         { return c.Store(r) }
func StoreStack(c *Calc, r string) error    { return c.StoreStack(r) }
func Load(c *Calc, r string) error          { return c.Load(r) }

func ResetMacro(c *Calc, m string) error              { return c.ResetMacro(m) }
func ResetMacros(c *Calc) error                       { return c.ResetMacros() }
func StoreMacro(c *Calc, m string, args ...any) error { return c.StoreMacro(m, args...) }
func Exec(c *Calc, m Macro)                           { c.Exec(m) }
func ExecMacro(c *Calc, m string) error               { return c.ExecMacro(m) }

func If(c *Calc, e1, e2 MacroElement) error { return c.If(e1, e2) }

func Abs(c *Calc) error   { return c.Abs() }
func Neg(c *Calc) error   { return c.Neg() }
func Inc(c *Calc) error   { return c.Inc() }
func Dec(c *Calc) error   { return c.Dec() }
func Inv(c *Calc) error   { return c.Inv() }
func Add(c *Calc) error   { return c.Add() }
func Sub(c *Calc) error   { return c.Sub() }
func Mul(c *Calc) error   { return c.Mul() }
func Div(c *Calc) error   { return c.Div() }
func Quo(c *Calc) error   { return c.Quo() }
func Rem(c *Calc) error   { return c.Rem() }
func Pow(c *Calc) error   { return c.Pow() }
func Sqrt(c *Calc) error  { return c.Sqrt() }
func Sqrtn(c *Calc) error { return c.Sqrtn() }
func Fact(c *Calc) error  { return c.Fact() }

func Cmp(c *Calc) error { return c.Cmp() }
func Eq(c *Calc) error  { return c.Eq() }
func Ne(c *Calc) error  { return c.Ne() }
func Gt(c *Calc) error  { return c.Gt() }
func Ge(c *Calc) error  { return c.Ge() }
func Lt(c *Calc) error  { return c.Lt() }
func Le(c *Calc) error  { return c.Le() }

func ToBase(c *Calc, b int) error    { return c.ToBase(b) }
func ToInt(c *Calc, b ...int) error  { return c.ToInt(b...) }
func ToDec(c *Calc, b ...int) error  { return c.ToDec(b...) }
func ToSci(c *Calc, b ...int) error  { return c.ToSci(b...) }
func ToFrac(c *Calc, b ...int) error { return c.ToFrac(b...) }

func Lsh(c *Calc) error { return c.Lsh() }
func Rsh(c *Calc) error { return c.Rsh() }

func Sum(c *Calc) error          { return c.Sum() }
func Len(c *Calc) error          { return c.Len() }
func Min(c *Calc) error          { return c.Min() }
func Max(c *Calc) error          { return c.Max() }
func Mean(c *Calc) error         { return c.Mean() }
func Median(c *Calc) error       { return c.Median() }
func Mode(c *Calc) error         { return c.Mode() }
func Variance(c *Calc) error     { return c.Variance() }
func StdDeviation(c *Calc) error { return c.StdDeviation() }
