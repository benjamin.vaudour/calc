package calc

import (
	"fmt"
)

const (
	errEmpty    = "%s est vide"
	errNotExist = "%s n’existe pas"
	errNeed2Nbs = "%s contient moins de 2 éléments"
	errUnknown  = "commande inconnue : '%s'"
	errStack    = "La pile"
	errReg      = "Le registre '%s'"
	errMemory   = "La mémoire"
	errMacro    = "La macro '%s'"
	errStayArgs = "la liste des arguments restants à parser"
	errCallback = "La fonction de callback n’est pas définie"
	errParser   = "La fonction de parsage n’est pas définie"
)

func ErrIsEmpty(s string) error {
	return fmt.Errorf(errEmpty, s)
}

func ErrNotExist(s string) error {
	return fmt.Errorf(errNotExist, s)
}

func ErrLenLt2(s string) error {
	return fmt.Errorf(errNeed2Nbs, s)
}

func ErrIsUnknow(s string) error {
	return fmt.Errorf(errUnknown, s)
}

func reg(s string) string {
	return fmt.Sprintf(errReg, s)
}

func mac(s string) string {
	return fmt.Sprintf(errMacro, s)
}
