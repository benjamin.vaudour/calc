package calc

import "framagit.org/benjamin.vaudour/number/v2"

type Calc struct {
	s Stack
	r Registry
	m Macros
}

func New() *Calc {
	return &Calc{
		r: make(Registry),
		m: make(Macros),
	}
}

func (c *Calc) SetPrecision(precision *uint64, fix *bool) {
	if precision != nil {
		number.FloatingPrecision = *precision
	}

	if fix != nil {
		number.FixedPrecision = *fix
	}
}

func (c *Calc) Precision() (precision uint64, fix bool) {
	return number.FloatingPrecision, number.FixedPrecision
}

func (c *Calc) Push(numbers ...number.Number) {
	c.s.Add(numbers...)
}

func (c *Calc) Stack() Stack {
	return c.s.Clone()
}

func (c *Calc) GetStack() (stk Stack, err error) {
	if c.s.IsEmpty() {
		err = ErrIsEmpty(errStack)
	} else {
		stk = c.Stack()
	}
	return
}

func (c *Calc) Last() (number.Number, error) {
	return c.s.Last()
}

func (c *Calc) Pop() error {
	_, err := c.s.Pop()
	return err
}

func (c *Calc) Reset() error {
	return c.s.Reset()
}

func (c *Calc) Duplicate() error {
	return c.s.Dup()
}

func (c *Calc) DuplicateStack() error {
	return c.s.DupAll()
}

func (c *Calc) Reverse() error {
	return c.s.Rev()
}

func (c *Calc) ReverseStack() error {
	return c.s.RevAll()
}

func (c *Calc) Sort() error {
	return c.s.Sort()
}

func (c *Calc) SortReverse() error {
	return c.s.SortReverse()
}

func (c *Calc) Registries() Registry {
	return c.r.Clone()
}

func (c *Calc) GetRegistries() (r Registry, err error) {
	if len(c.r) == 0 {
		err = ErrIsEmpty(errMemory)
	} else {
		r = c.Registries()
	}
	return
}

func (c *Calc) Registry(k string) (Stack, error) {
	return c.r.Get(k)
}

func (c *Calc) ResetRegistries() error {
	return c.r.ResetAll()
}

func (c *Calc) ResetRegistry(k string) error {
	return c.r.Reset(k)
}

func (c *Calc) Store(k string) error {
	n, err := c.s.Pop()
	if err == nil {
		c.r.Set(k, n)
	}
	return err
}

func (c *Calc) StoreStack(k string) error {
	if c.s.IsEmpty() {
		return ErrIsEmpty(errStack)
	}
	c.r.Set(k, c.Stack()...)
	return nil
}

func (c *Calc) Load(k string) error {
	s, err := c.r.Get(k)
	if err == nil {
		c.Push(s...)
	}
	return err
}

func (c *Calc) Macros() Macros {
	return c.m.Clone()
}

func (c *Calc) GetMacros() (m Macros, err error) {
	if len(c.m) == 0 {
		err = ErrIsEmpty(errMemory)
	} else {
		m = c.Macros()
	}
	return
}

func (c *Calc) Macro(k string) (Macro, error) {
	return c.m.Get(k)
}

func (c *Calc) StoreMacro(k string, args ...any) error {
	return c.m.Set(k, args...)
}

func (c *Calc) Exec(m Macro) {
	for _, e := range m {
		if e.IsNumber() {
			c.Push(e.n)
		} else {
			e.c(c)
		}
	}
}

func (c *Calc) ExecMacro(k string) error {
	m, err := c.m.Get(k)
	if err == nil {
		c.Exec(m)
	}
	return err
}

func (c *Calc) ResetMacro(k string) error {
	return c.m.Reset(k)
}

func (c *Calc) ResetMacros() error {
	return c.m.ResetAll()
}

func (c *Calc) NoAction() error {
	return nil
}

func (c *Calc) If(e1, e2 MacroElement) error {
	n, err := c.s.Pop()
	if err == nil {
		if n.IsZero() || n.IsNan() {
			if e2.IsNumber() {
				c.Push(e2.n)
			} else if e2.IsCommand() {
				e2.c(c)
			}
		}
	}
	return err
}
