package main

import (
	"fmt"
	"os"

	"framagit.org/benjamin.vaudour/calc/cli"
	"framagit.org/benjamin.vaudour/readline"
)

func main() {
	calc, rl := cli.New(), readline.New()
	for {
		args, err := rl.Prompt("> ")
		if err != nil {
			fmt.Printf("Erreur d’entrée: %s\n", err)
			os.Exit(1)
		}
		calc.ExecAll(args)
	}
}
