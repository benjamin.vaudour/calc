package calc

import (
	"fmt"

	"framagit.org/benjamin.vaudour/number/v2"
)

type CalcFunc func(*Calc)

type MacroElement struct {
	n number.Number
	c CalcFunc
	t bool
}

func (e MacroElement) IsNumber() bool {
	return !e.t
}

func (e MacroElement) IsCommand() bool {
	return e.t
}

func MNumber(n number.Number) MacroElement {
	return MacroElement{n: n}
}

func MCommand(c CalcFunc) MacroElement {
	return MacroElement{c: c, t: true}
}

type Macro []MacroElement

func (m Macro) Clone() Macro {
	out := make(Macro, len(m))
	copy(out, m)
	return out
}

func NewMacro(args ...any) (m Macro, err error) {
	for _, e := range args {
		if n, ok := e.(number.Number); ok {
			m = append(m, MNumber(n))
		} else if c, ok := e.(CalcFunc); ok {
			m = append(m, MCommand(c))
		} else {
			err = ErrIsUnknow(fmt.Sprint(e))
			break
		}
	}
	if err != nil {
		m = nil
	}
	return
}

type Macros map[string]Macro

func (m Macros) Exists(k string) (ok bool) {
	_, ok = m[k]
	return
}

func (m Macros) SetMacro(k string, mm Macro) {
	m[k] = mm
}

func (m Macros) Set(k string, args ...any) error {
	mm, err := NewMacro(args...)
	if err == nil {
		m.SetMacro(k, mm)
	}
	return err
}

func (m Macros) Get(k string) (mm Macro, err error) {
	if m.Exists(k) {
		mm = m[k].Clone()
	} else {
		err = ErrNotExist(mac(k))
	}
	return
}

func (m Macros) Reset(k string) (err error) {
	if m.Exists(k) {
		delete(m, k)
	} else {
		err = ErrNotExist(mac(k))
	}
	return
}

func (m Macros) ResetAll() (err error) {
	if len(m) > 0 {
		for k := range m {
			delete(m, k)
		}
	} else {
		err = ErrIsEmpty(errMemory)
	}
	return
}

func (m Macros) Clone() (out Macros) {
	out = make(Macros)
	for k, mm := range m {
		out[k] = mm.Clone()
	}
	return
}
