package calc

import (
	"sort"

	"framagit.org/benjamin.vaudour/number/v2"
)

type Stack []number.Number

func (s *Stack) Len() int {
	return len(*s)
}

func (s *Stack) I1() int {
	return s.Len() - 1
}

func (s *Stack) I2() int {
	return s.Len() - 2
}

func (s *Stack) IsEmpty() bool {
	return s.Len() == 0
}

func (s Stack) Clone() (out Stack) {
	out = make(Stack, s.Len())
	copy(out, s)
	return
}

func (s *Stack) Last() (n number.Number, err error) {
	i := s.I1()
	if i >= 0 {
		n = (*s)[i]
	} else {
		err = ErrIsEmpty(errStack)
	}
	return
}

func (s *Stack) Last2() (n1, n2 number.Number, err error) {
	i := s.I2()
	if i >= 0 {
		n1, n2 = (*s)[i], (*s)[i+1]
	} else {
		ErrLenLt2(errStack)
	}
	return
}

func (s *Stack) Pop() (n number.Number, err error) {
	if n, err = s.Last(); err == nil {
		i := s.I1()
		*s = (*s)[:i]
	}
	return
}

func (s *Stack) Pop2() (n1, n2 number.Number, err error) {
	if n1, n2, err = s.Last2(); err == nil {
		i := s.I2()
		*s = (*s)[:i]
	}
	return
}

func (s *Stack) Reset() (err error) {
	l := s.Len()
	if l > 0 {
		*s = (*s)[:0]
	} else {
		err = ErrIsEmpty(errStack)
	}
	return
}

func (s *Stack) Add(numbers ...number.Number) {
	*s = append(*s, numbers...)
}

func (s *Stack) Dup() (err error) {
	var n number.Number
	if n, err = s.Last(); err == nil {
		s.Add(n)
	}
	return
}

func (s *Stack) DupAll() (err error) {
	if !s.IsEmpty() {
		s.Add((*s)...)
	} else {
		err = ErrIsEmpty(errStack)
	}
	return
}

func (s *Stack) Swap(i, j int) {
	(*s)[i], (*s)[j] = (*s)[j], (*s)[i]
}

func (s *Stack) Rev() (err error) {
	i := s.I2()
	if i >= 0 {
		s.Swap(i, i+1)
	} else {
		err = ErrLenLt2(errStack)
	}
	return
}

func (s *Stack) RevAll() (err error) {
	l := s.Len()
	l2 := l >> 1
	if l2 > 0 {
		for i := 0; i < l2; i++ {
			s.Swap(i, l-1-i)
		}
	} else {
		err = ErrLenLt2(errStack)
	}
	return
}

func (s *Stack) Sort() error {
	if s.IsEmpty() {
		return ErrIsEmpty(errStack)
	}
	sort.Slice(*s, func(i, j int) bool {
		return number.Lt((*s)[i], (*s)[j])
	})
	return nil
}

func (s *Stack) SortReverse() error {
	if s.IsEmpty() {
		return ErrIsEmpty(errStack)
	}
	sort.Slice(*s, func(i, j int) bool {
		return number.Gt((*s)[i], (*s)[j])
	})
	return nil
}
