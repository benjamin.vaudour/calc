package calc

import "framagit.org/benjamin.vaudour/number/v2"

type Registry map[string]Stack

func (r Registry) Exists(k string) (ok bool) {
	_, ok = r[k]
	return
}

func (r Registry) Set(k string, numbers ...number.Number) {
	r[k] = numbers
}

func (r Registry) Get(k string) (s Stack, err error) {
	var ok bool
	if s, ok = r[k]; ok {
		s = s.Clone()
	} else {
		err = ErrNotExist(reg(k))
	}
	return
}

func (r Registry) Reset(k string) (err error) {
	if r.Exists(k) {
		delete(r, k)
	} else {
		err = ErrNotExist(reg(k))
	}
	return
}

func (r Registry) ResetAll() (err error) {
	if len(r) > 0 {
		for k := range r {
			delete(r, k)
		}
	} else {
		err = ErrIsEmpty(errMemory)
	}
	return
}

func (r Registry) Clone() (out Registry) {
	out = make(Registry)
	for k, s := range r {
		out[k] = s.Clone()
	}
	return
}
