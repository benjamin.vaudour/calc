package cli

import (
	"bufio"
	"fmt"
	"strings"

	"framagit.org/benjamin.vaudour/calc"
	"framagit.org/benjamin.vaudour/number/v2"
)

type Cli struct {
	c       *calc.Calc
	macros  map[string]string
	macro   string
	cmds    []any
	needAlt bool
	alt     []calc.MacroElement
}

func (c *Cli) macroBegan() bool {
	return c.macro != ""
}

func (c *Cli) addMacro(arg string) {
	c.macro = fmt.Sprintf("%s %s", c.macro, arg)
}

func (c *Cli) addMacroElement(e any, v string) {
	c.cmds = append(c.cmds, e)
	c.addMacro(v)
}

func (c *Cli) parse(arg string) (n number.Number, f calcErrFunc, isNb bool, err error) {
	if cb, exists := calcFunctions[arg]; exists {
		f = cb
	} else if cb, exists := cliFuncions[arg]; exists {
		f = cli2Calc(cb, c)
	} else if cb, w, exists := searchWord(arg); exists {
		f = word2Calc(cb, w)
	} else if cb, d, exists := searchInt(arg); exists {
		f = int2Calc(cb, d)
	} else if cb, m, exists := searchMacro(arg); exists {
		f = macro2Calc(cb, c, m)
	} else if n, isNb = number.Parse(arg); !isNb {
		err = fmt.Errorf("Commande inconnue: %s", arg)
	}
	return
}

func (c *Cli) resetAlt() {
	c.needAlt, c.alt = false, c.alt[:0]
}

func (c *Cli) resetMacro() {
	c.macro, c.cmds = "", c.cmds[:0]
}

func (c *Cli) resetAll() {
	c.resetAlt()
	c.resetMacro()
	c.c.Reset()
}

func (c *Cli) Exec(arg string) {
	n, cb, isNumber, err := c.parse(arg)
	switch {
	case err != nil:
		printError(err)
		c.resetAll()
	case c.macroBegan():
		if isNumber {
			c.addMacroElement(n, arg)
		} else {
			c.addMacroElement(cb, arg)
		}
	case c.needAlt:
		var me calc.MacroElement
		if isNumber {
			me = calc.MNumber(n)
		} else {
			me = calc.MCommand(calcErr2Calc(cb))
		}
		c.alt = append(c.alt, me)
		if len(c.alt) == 2 {
			if err := c.c.If(c.alt[0], c.alt[1]); err != nil {
				printError(err)
			}
			c.resetAlt()
		}
	default:
		if isNumber {
			c.c.Push(n)
		} else {
			calcErr2Calc(cb)(c.c)
		}
	}
}

func (c *Cli) ExecAll(args string) {
	sc := bufio.NewScanner(strings.NewReader(args))
	sc.Split(bufio.ScanWords)
	for sc.Scan() {
		arg := sc.Text()
		c.Exec(arg)
	}
}

func New() *Cli {
	c := Cli{
		c:      calc.New(),
		macros: make(map[string]string),
	}
	return &c
}
