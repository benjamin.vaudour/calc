package cli

import (
	"errors"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"

	"framagit.org/benjamin.vaudour/calc"
)

type (
	errFunc      func() error
	calcErrFunc  func(*calc.Calc) error
	cliFunc      func(*Cli)
	cliErrFunc   func(*Cli) error
	wordErrFunc  = func(*calc.Calc, string) error
	intErrFunc   = func(*calc.Calc, int) error
	macroErrFunc = func(*Cli, string) error
)

func calcErr2Calc(cb calcErrFunc) calc.CalcFunc {
	return func(c *calc.Calc) {
		if err := cb(c); err != nil {
			fmt.Printf("\033[1;31m%s\033[m\n", err)
		}
	}
}

func printResult(arg any) error {
	_, err := fmt.Printf("\033[1;33m%s\033[m\n", arg)
	return err
}

func printError(err error) error {
	_, err = fmt.Printf("\033[1;31m%s\033[m\n", err)
	return err
}

func f2Calc(cb errFunc) calcErrFunc {
	return func(_ *calc.Calc) error {
		return cb()
	}
}

func cli2Calc(cb cliErrFunc, c *Cli) calcErrFunc {
	return func(_ *calc.Calc) error {
		return cb(c)
	}
}

func word2Calc(cb wordErrFunc, w string) calcErrFunc {
	return func(c *calc.Calc) error {
		return cb(c, w)
	}
}

func int2Calc(cb intErrFunc, d int) calcErrFunc {
	return func(c *calc.Calc) error {
		return cb(c, d)
	}
}

func macro2Calc(cb macroErrFunc, c *Cli, w string) calcErrFunc {
	return func(_ *calc.Calc) error {
		return cb(c, w)
	}
}

func stack2Str(numbers calc.Stack) string {
	sl := make([]string, len(numbers))
	for i, n := range numbers {
		sl[i] = n.String()
	}
	return strings.Join(sl, " ")
}

func quit() error {
	os.Exit(0)
	return nil
}

func printLine(k string, arg any) error {
	_, err := fmt.Printf("\033[1;32m%s\033[m → \033[1;33m%s\033[m\n", k, arg)
	return err
}

func printHelp() error {
	_, err := fmt.Print(help)
	return err
}

func printOption(c *calc.Calc) error {
	p, f := c.Precision()
	t := "auto"
	if f {
		t = "fixe"
	}
	return printResult(fmt.Sprintf("%d (%s)", p, t))
}

func printLast(c *calc.Calc) error {
	n, err := c.Last()
	if err == nil {
		return printResult(n)
	}
	return err
}

func printStack(c *calc.Calc) error {
	stk, err := c.GetStack()
	if err == nil {
		return printResult(stack2Str(stk))
	}
	return err
}

func printRegistries(c *calc.Calc) error {
	r, err := c.GetRegistries()
	if err == nil {
		for k, s := range r {
			printLine(k, stack2Str(s))
		}
	}
	return err
}

func printRegistry(c *calc.Calc, k string) error {
	r, err := c.Registry(k)
	if err == nil {
		return printResult(stack2Str(r))
	}
	return err
}

func printMacros(c *Cli) error {
	_, err := c.c.GetMacros()
	if err == nil {
		for k, m := range c.macros {
			printLine(k, m)
		}
	}
	return err
}

func printMacro(c *Cli, k string) error {
	_, err := c.c.Macro(k)
	if err == nil {
		return printResult(c.macros[k])
	}
	return err
}

func beginMacro(c *Cli) error {
	if c.macroBegan() {
		c.resetAll()
		return errors.New("Les macros imbriquées ne sont pas supportées")
	}
	c.macro = "["
	return nil
}

func resetMacro(c *Cli, k string) error {
	err := c.c.ResetMacro(k)
	if err == nil {
		delete(c.macros, k)
	}
	return err
}

func storeMacro(c *Cli, k string) error {
	if len(c.cmds) == 0 {
		c.resetAll()
		return errors.New("La macro n’a pas commencé")
	}
	c.addMacro("]")
	err := c.c.StoreMacro(k, c.cmds...)
	if err == nil {
		c.macros[k] = c.macro
	}
	c.resetMacro()
	return err
}

func precision(p *int, fix bool) calcErrFunc {
	var pp uint64
	if p != nil {
		pp = uint64(*p)
	}
	return func(c *calc.Calc) error {
		c.SetPrecision(&pp, &fix)
		return nil
	}
}

func precision2(fix bool) intErrFunc {
	return func(c *calc.Calc, p int) error {
		return precision(&p, fix)(c)
	}
}

func conv(cb func(*calc.Calc, ...int) error, b ...int) calcErrFunc {
	return func(c *calc.Calc) error {
		return cb(c, b...)
	}
}

func conv2(cb func(*calc.Calc, ...int) error) intErrFunc {
	return func(c *calc.Calc, d int) error {
		return cb(c, d)
	}
}

func base(b int) calcErrFunc {
	return func(c *calc.Calc) error {
		return calc.ToBase(c, b)
	}
}

func conditional(c *Cli) error {
	if c.needAlt {
		c.resetAll()
		return errors.New("Les conditions imbriquées ne sont pas supportées")
	}
	c.needAlt = true
	return nil
}

var (
	calcFunctions = map[string]calcErrFunc{
		"h":   f2Calc(printHelp),
		"q":   f2Calc(quit),
		"op":  printOption,
		"of":  precision(nil, true),
		"oa":  precision(nil, false),
		"p":   printLast,
		"P":   printStack,
		"c":   calc.Pop,
		"C":   calc.Reset,
		"d":   calc.Duplicate,
		"D":   calc.DuplicateStack,
		"r":   calc.Reverse,
		"R":   calc.ReverseStack,
		"s":   calc.Sort,
		"S":   calc.SortReverse,
		":P":  printRegistries,
		":C":  calc.ResetRegistries,
		";C":  calc.ResetMacros,
		"|":   calc.Abs,
		"++":  calc.Inc,
		"--":  calc.Dec,
		"inv": calc.Inv,
		"+":   calc.Add,
		"-":   calc.Sub,
		"*":   calc.Mul,
		"×":   calc.Mul,
		"/":   calc.Div,
		"÷":   calc.Div,
		"//":  calc.Quo,
		"%":   calc.Rem,
		"^":   calc.Pow,
		"v":   calc.Sqrt,
		"vn":  calc.Sqrtn,
		"!":   calc.Fact,
		"<=>": calc.Cmp,
		"=":   calc.Eq,
		"<>":  calc.Ne,
		"≠":   calc.Ne,
		">":   calc.Gt,
		">=":  calc.Ge,
		"≥":   calc.Ge,
		"<=":  calc.Le,
		"≤":   calc.Le,
		"cb":  base(2),
		"co":  base(8),
		"cx":  base(16),
		"ci":  conv(calc.ToInt),
		"cd":  conv(calc.ToDec),
		"cs":  conv(calc.ToSci),
		"cf":  conv(calc.ToFrac),
		">>":  calc.Rsh,
		"<<":  calc.Lsh,
		"min": calc.Min,
		"max": calc.Max,
		"sum": calc.Sum,
		"moy": calc.Mean,
		"med": calc.Median,
		"mod": calc.Mode,
		"var": calc.Variance,
		"dev": calc.StdDeviation,
		"l":   calc.Len,
	}

	cliFuncions = map[string]cliErrFunc{
		";P": printMacros,
		"[":  beginMacro,
		"?":  conditional,
	}

	wordFunctions = map[string]wordErrFunc{
		`^:p\w+$`: printRegistry,
		`^:c\w+$`: calc.ResetRegistry,
		`^:l\w+$`: calc.Load,
		`^:s\w+$`: calc.Store,
		`^:S\w+$`: calc.StoreStack,
		`^;x\w+$`: calc.ExecMacro,
	}

	intFunctions = map[string]intErrFunc{
		`^c\d+$`:  calc.ToBase,
		`^ci\d+$`: conv2(calc.ToInt),
		`^cd\d+$`: conv2(calc.ToDec),
		`^cs\d+$`: conv2(calc.ToSci),
		`^cf\d+$`: conv2(calc.ToFrac),
		`^of\d+$`: precision2(true),
		`^oa\d+$`: precision2(false),
	}

	macroFunctions = map[string]macroErrFunc{
		`^;p\w+$`: printMacro,
		`^;c\w+$`: resetMacro,
		`^\]\w+$`: storeMacro,
	}
)

func searchWord(arg string) (cb wordErrFunc, word string, ok bool) {
	for k, f := range wordFunctions {
		if ok = regexp.MustCompile(k).MatchString(arg); ok {
			cb, word = f, arg[2:]
			break
		}
	}
	return

}

func searchInt(arg string) (cb intErrFunc, d int, ok bool) {
	for k, f := range intFunctions {
		if ok = regexp.MustCompile(k).MatchString(arg); ok {
			var err error
			if d, err = strconv.Atoi(arg[1:]); err != nil {
				d, _ = strconv.Atoi(arg[2:])
			}
			cb = f
			break
		}
	}
	return
}

func searchMacro(arg string) (cb macroErrFunc, macro string, ok bool) {
	for k, f := range macroFunctions {
		if ok = regexp.MustCompile(k).MatchString(arg); ok {
			cb, macro = f, arg[2:]
			if arg[0] == ']' {
				macro = arg[1:]
			}
			break
		}
	}
	return

}
