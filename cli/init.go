package cli

import (
	_ "embed"
	"strings"
)

//go:embed resources/help.txt
var embedHelp []byte

var help string

func init() {
	help = string(embedHelp)
	help = strings.ReplaceAll(help, "{T}", "\033[1;33m")
	help = strings.ReplaceAll(help, "{0}", "\033[m")
	help = strings.ReplaceAll(help, "{B}", "\033[33m")
}
