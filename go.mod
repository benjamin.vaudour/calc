module framagit.org/benjamin.vaudour/calc

go 1.18

require (
	framagit.org/benjamin.vaudour/number/v2 v2.0.0
	framagit.org/benjamin.vaudour/readline v1.0.0
)

require (
	framagit.org/benjamin.vaudour/collection/v2 v2.1.1 // indirect
	framagit.org/benjamin.vaudour/converter/v2 v2.0.3 // indirect
	framagit.org/benjamin.vaudour/shell/v2 v2.0.0 // indirect
)
